import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class kenbaiki {
    private JPanel root;
    private JButton shoyuButton;
    private JButton sioButton;
    private JButton misoButton;
    private JButton iekeiButton;
    private JButton mokotanmenButton;
    private JButton tomatoButton;
    private JTextPane textPane1;
    private JButton checkOutButton;
    private JTextPane textPane2;
    private JButton bigSizeButton;
    private JButton chashuButton;
    private JButton redoingOrdersButton;
    private JButton cancellationOfOrderButton;

    int sum = 0;

    void order(String food) {
        int nedan = 0;

        if (food.equals("Shoyu")) {
            nedan = 500;
        }
        if (food.equals("Sio")) {
            nedan = 550;
        }
        if (food.equals("Miso")) {
            nedan = 600;
        }
        if (food.equals("Iekei")) {
            nedan = 650;
        }
        if (food.equals("Mokotanmen")) {
            nedan = 700;
        }
        if (food.equals("Tomato")) {
            nedan = 750;
        }
        if (food.equals("BigSize")) {
            nedan = 50;
        }
        if (food.equals("Chashu")) {
            nedan = 100;
        }
        int confirmation =
                JOptionPane.showConfirmDialog(null,
                        "Would you like to order " + food + "?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.");
            String currentText = textPane1.getText();
            textPane1.setText(currentText + "" + food + "  " + nedan + "yen" + "\n");
            sum+=nedan;
            textPane2.setText("Total   "+sum+"yen" + "\n");
        }
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("kenbaiki");
        frame.setContentPane(new kenbaiki().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


    }

    public kenbaiki() {
        textPane2.setText("Total   "+sum+"yen" + "\n");

        shoyuButton.setIcon(new ImageIcon(this.getClass().getResource("shoyu.JPG")));
        sioButton.setIcon(new ImageIcon(this.getClass().getResource("sio.JPG")));
        misoButton.setIcon(new ImageIcon(this.getClass().getResource("miso.JPG")));
        iekeiButton.setIcon(new ImageIcon(this.getClass().getResource("ie.JPG")));
        mokotanmenButton.setIcon(new ImageIcon(this.getClass().getResource("moko.JPG")));
        tomatoButton.setIcon(new ImageIcon(this.getClass().getResource("tomato.JPG")));

        shoyuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Shoyu");
            }
        });
        sioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sio");
            }
        });
        misoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Miso");
            }
        });
        iekeiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Iekei");
            }
        });
        mokotanmenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mokotanmen");
            }
        });
        tomatoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tomato");
            }
        });

        chashuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chashu");
            }
        });
        bigSizeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("BigSize");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int confirmation =
                        JOptionPane.showConfirmDialog(null,
                                "Would you like to order checkout?",
                                "Checkout Confirmation",
                                JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+sum+"yen.");
                    textPane1.setText("");
                    sum=0;
                    textPane2.setText("Total   "+sum+"yen" + "\n");
                    JOptionPane.showMessageDialog(null, "The total price is "+sum+"yen.");

                }

            }
        });
        redoingOrdersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation =
                        JOptionPane.showConfirmDialog(null,
                                "Would you like to redo your order?",
                                "Redoing Confirmation",
                                JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "All orders have been cancelled.");
                    textPane1.setText("");
                    sum = 0;
                    textPane2.setText("Total   " + sum + "yen" + "\n");
                    JOptionPane.showMessageDialog(null, "Please order.");
                }
            }
        });
    }
}
